import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { JoueurComponent } from './joueur/joueur.component';
import { OrdinateurComponent } from './ordinateur/ordinateur.component';
import { ArbitreComponent } from './arbitre/arbitre.component';
import { AppRoutingModule } from './app-routing.module';
import { TotoComponent } from './toto/toto.component';
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    JoueurComponent,
    OrdinateurComponent,
    ArbitreComponent,
    TotoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
