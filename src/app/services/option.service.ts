import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  computerOptionSource: Subject<boolean> = new Subject<boolean>();
  playerOptionSource: Subject<boolean> = new Subject<boolean>();

  computerOption$ = this.computerOptionSource.asObservable();
  playerOption$ = this.playerOptionSource.asObservable();

  constructor() { }

  setComputerOption(b: boolean) {
    this.computerOptionSource.next(b);
  }

  setPlayerOption(b: boolean) {
    this.playerOptionSource.next(b);
  }
}
