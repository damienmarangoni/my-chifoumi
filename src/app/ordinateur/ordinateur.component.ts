import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CHOICES } from '../rules'
import { OptionService } from "../services/option.service";
import { tap, map } from 'rxjs/operators'
import { Subscription , Observable} from 'rxjs';

@Component({
  selector: 'app-ordinateur',
  templateUrl: './ordinateur.component.html',
  styleUrls: ['./ordinateur.component.css']
})
export class OrdinateurComponent implements OnInit {

  @Output() onChoice: EventEmitter<string> = new EventEmitter<string>();
  @Output() onObs: EventEmitter<string> = new EventEmitter<string>();

  // obs: Observable<boolean>;
  sub: Subscription;

  choice: string = "????"

  isDisabled: boolean = true;

  constructor(private optionService: OptionService) { }

  ngOnInit() {
    this.sub = this.optionService.computerOption$.pipe(
      tap(_ => console.log("Enable computer button")),
      map(val => val)
    ).subscribe(data => 
      {
        this.isDisabled = data;
        
        this.onObs.emit("ok");
      });
    //this.obs = this.optionService.computerOption$;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  startChoice() {
    this.choice = CHOICES[Math.floor(Math.random() * (2 - 1 + 1) ) + 1];
    this.onChoice.emit(this.choice)
    this.isDisabled = true;
    this.optionService.playerOptionSource.next(true);
  }

}
