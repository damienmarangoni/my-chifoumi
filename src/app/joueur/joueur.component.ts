import { Component, OnInit } from '@angular/core';
import { CHOICES } from '../rules'
import { OptionService } from "../services/option.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-joueur',
  templateUrl: './joueur.component.html',
  styleUrls: ['./joueur.component.css'],
  // providers: [OptionService]
})
export class JoueurComponent implements OnInit {

sub: Subscription;

  choices: string[] = CHOICES;
  myChoice: string = "????";
  isDisabled: boolean = false;

  constructor(private optionService: OptionService) { }

  ngOnInit() {
    this.sub = this.optionService.playerOption$.subscribe(data => 
      {
        this.isDisabled = data;
        console.log("Disable player buttons")});
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  setChoice(s: string) {
    this.myChoice = s;
    this.optionService.setComputerOption(false);
  }

}
