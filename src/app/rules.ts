export const CHOICES = ["Pierre", "Feuille", "Ciseaux"];
export const RESULTS = {
    "Pierre-Feuille": "Ordinateur",
    "Pierre-Ciseaux": "Joueur",
    "Pierre-Pierre": "Egalite",
    "Feuille-Pierre": "Joueur",
    "Feuille-Ciseaux": "Ordinateur",
    "Feuille-Feuille": "Egalite",
    "Ciseaux-Pierre": "Ordinateur",
    "Ciseaux-Feuille": "Joueur",
    "Ciseaux-Ciseaux": "Egalite"
}