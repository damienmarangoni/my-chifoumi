import { Component, OnInit, ViewChild } from '@angular/core';
import { JoueurComponent } from "../joueur/joueur.component";
import { OrdinateurComponent } from "../ordinateur/ordinateur.component";
import { RESULTS } from "../rules";

@Component({
  selector: 'app-arbitre',
  templateUrl: './arbitre.component.html',
  styleUrls: ['./arbitre.component.css']
})
export class ArbitreComponent implements OnInit {

  @ViewChild(JoueurComponent) joueur : JoueurComponent;
  @ViewChild(OrdinateurComponent) computer : OrdinateurComponent;

  winner: string = "????"
  playerResult: string;
  computerResult: string;

  constructor() { }

  ngOnInit() {
  }

  analyseResults(s: string) {
    this.playerResult = this.joueur.myChoice;
    this.computerResult = s;

    this.winner = RESULTS[this.playerResult + "-" + this.computerResult]
  }

  reset() {
    console.log("reset");
    this.joueur.myChoice = "????";
    this.joueur.isDisabled = false;
    this.computer.choice = "????";
    this.computer.isDisabled = true;
    this.winner = "????";
  }

  obsReceived(s: string) {
    console.log("observable has been received =>" + s);
  }

}
